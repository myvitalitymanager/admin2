module.exports = function(grunt) {
    grunt.initConfig({
        connect: {
            lr: {
                options: {
                    port: 8100,
                    base: 'client',
                    livereload: true
                }
            },
            dist: {
                options: {
                    port: 8100,
                    base: 'client',
                    keepalive: true
                }
            }
        },
        watch: {
            wire: {
                options: {
                    event: ['added', 'deleted']
                },
                files: ['client/**/*', '!client/bower_components/**/*', 'bower.json'],
                tasks: ['wiredep:dev', 'injector']
            },
            sass: {
                files: ['client/app/**/*.scss', 'client/components/**/*.scss'],
                tasks: ['sass:dev']
            },
            reload: {
                files: ['client/**/*', '!client/bower_components/**/*', 'bower.json'],
                options: {
                    livereload: true
                }
            }
        },
        sass: {
            dev: {
                files: {
                    'client/assets/app.css': 'client/app/app.scss'
                }
            }
        },
        injector: {
            options: {},
            js: {
                options: {
                    transform: function(filePath) {
                        filePath = filePath.replace('/client/', '');
                        switch(filePath.substring(filePath.lastIndexOf('.') + 1)) {
                            case 'js': return '<script src="' + filePath + '"></script>';
                            case 'css': return '<link rel="stylesheet" href="' + filePath + '"/>';
                        }
                    },
                    ignorePath: ['client/app/settings.sample.js']
                },
                files: {
                    'client/index.html': [
                      'client/app/app.js',
                      'client/components/settings/settings.service.js',
                      'client/{app,components}/**/*.js',
                      'client/assets/**/*.css',
                      '!client/**/*.spec.js'
                    ],
                    'client/app/app.scss': ['client/app/**/*.scss']
                }
            },
            scss: {
                options: {
                    transform: function(filePath) {
                        filePath = filePath.replace('/client/app/', '');
                        filePath = filePath.replace('/client/components/', '../components/');
                        return '@import \'' + filePath + '\';';
                    },
                    starttag: '// injector:scss',
                    endtag: '// endinjector'
                },
                files: {
                    'client/app/app.scss': [
                        'client/{app,components}/**/*.scss',
                        '!client/app/app.scss'
                    ]
                }
            }
        },
        wiredep: {
            dev: {
                src: [
                  'client/index.html',
                  'client/app/app.scss'
                ],

                options: {
                    devDependencies: true,
                    exclude: [/bootstrap-sass-official/, /bootstrap.js/, /bootstrap.css/, /font-awesome.css/, /angular-scenario/]
                }
            }
        }
    });

    grunt.registerTask('createconf', function() {
        // was: apiBaseUrl: '"+process.env.DEFAULT_API_PREFIX+"'\
        // prefix is no longer needed since admin-client is served from same nginx server-name
        grunt.file.write('client/app/settings.js',
            "'use strict';\
            angular.module('backofficeApp').config(function(settingsProvider) {\
                settingsProvider.settings.servers.unshift({\
                    name: 'Default',\
                    apiBaseUrl: ''\
                });\
            })"
        );
    });

    grunt.registerTask('serve', function(target) {
        if (target === 'dist') {
            return grunt.task.run(['sass:dev', 'createconf', 'connect:dist']);
        }
        return grunt.task.run([
            'wiredep:dev',
            'injector',
            'sass:dev',
            'connect:' + (target ? target : 'lr'),
            'watch'
        ]);
    });

    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-injector');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-wiredep');
}
