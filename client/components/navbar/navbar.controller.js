'use strict';

angular.module('backofficeApp')
  .controller('NavbarCtrl', function ($scope, $location, $state, auth) {
    $scope.menu = [
        {
            title: 'Home',
            state: 'main'
        },
        {
            title: 'Content',
            state: 'tree'
        },
        {
            title: 'Message',
            state: 'message'
        },
        {
            title: 'Modules',
            state: 'modules'
        },
        {
            title: 'Access',
            state: 'access.list'
        },
        {
            title: 'Data',
            state: 'data'
        }
    ];

    console.log(auth.getAuthenticatedUser());
    if((!auth.getAuthenticatedUser().canBackofficeAdmin && auth.getAuthenticatedUser().canEdit) ||
        (!auth.getAuthenticatedUser().canAdminister && auth.getAuthenticatedUser().canEdit)) {
        console.log(auth.getAuthenticatedUser().canBackofficeAdmin);
        $scope.menu = [
            {
                title: 'Home',
                state: 'main'
            },
            {
                title: 'Content',
                state: 'tree'
            }
        ];
    }

    $scope.isCollapsed = true;

    $scope.isActive = function(route) {
      return route === $location.path();
    };

    $scope.logout = function() {
      auth.setUnauthenticated();
      $state.go('auth.login');
    };
  });
