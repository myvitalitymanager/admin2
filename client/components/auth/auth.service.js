'use strict';

angular.module('backofficeApp')
  .provider('auth', function () {
    this.$get = function ($q, $localStorage, $resource, settings) {
      var Session = $resource(settings.apiBaseUrl + '/admin/sessions', null, {});

      function Auth() {
        if ($localStorage.session) {
          this.setAuthenticated($localStorage.session);
        }
      }

      Auth.prototype = {
        login: function(data) {
          var deferred = $q.defer();
          var self = this;

          var session = new Session({
            name: data.name,
            password: data.password
          });

          session.$save(function(session) {
            self.setAuthenticated(session);
            deferred.resolve(session);
          }, function(err) {
            deferred.reject(err);
          });

          return deferred.promise;
        },

        setAuthenticated: function(session) {
          this.session = session;
          $localStorage.session = session;
          return this;
        },

        setUnauthenticated: function() {
          this.session = null;
          $localStorage.session = null;

          return this;
        },

        getAuthenticatedUser: function() {
         // return this.session.data.name;
         return this.session.user;
        },

        isAuthenticated: function() {
          return this.session != null;
        },

        canAccess: function(state) {
          // Can always access the authentication pages
          if (/^auth/.test(state.name)) {
              return true;
          }
          // Needs to be authenticated to access in-app states.
          if (this.session) {
              return true;
          }
          return false;
        }
      };
      return new Auth();
    };
  });
