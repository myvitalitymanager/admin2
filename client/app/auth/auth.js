'use strict';

angular.module('backofficeApp')
  .config(function($stateProvider, $httpProvider, settingsProvider) {
    $stateProvider
    .state('auth', {
      abstract: true,
      url: '/auth',
      template: '<ui-view/>'
    })
    .state('auth.login', {
      url: '/login',
      controller: 'LoginCtrl',
      templateUrl: 'app/auth/login.html'
    });

    /*
     * Add the session id to every request which hits the api. Requires
     * the base64 function to encode the authorization header.
     */
    $httpProvider.interceptors.unshift(function($injector) {
      return {request: function(config) {
        var apiPrefix = settingsProvider.settings.apiBaseUrl;
        if (config.url.substring(0, apiPrefix.length) !== apiPrefix)
          return config;

        var auth = $injector.get('auth');
        if (auth.isAuthenticated()) {
          var id = auth.session.id;
          config.headers['Authorization'] = 'Basic ' + base64(id + ':');
        }
        return config;
      }};
    });

    /*
     * Intercept api errors to check wether the session has expired (http 401).
     */
     $httpProvider.interceptors.unshift(function($injector, $q) {
      return {responseError: function(res) {
        if (res.status === 401) {
          $injector.get('auth').setUnauthenticated();
          $injector.get('$state').go('auth.login');
        }
        return $q.reject(res);
      }};
    });

    // From http://phpjs.org/functions/base64_encode/
    function base64(data) {
      var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
      var o1, o2, o3, h1, h2, h3, h4, bits, i = 0, ac = 0, tmp = [];

      if (!data) {
        return data;
      }

      do { // pack three octets into four hexets
        o1 = data.charCodeAt(i++);
        o2 = data.charCodeAt(i++);
        o3 = data.charCodeAt(i++);

        bits = o1 << 16 | o2 << 8 | o3;

        h1 = bits >> 18 & 0x3f;
        h2 = bits >> 12 & 0x3f;
        h3 = bits >> 6 & 0x3f;
        h4 = bits & 0x3f;

        // use hexets to index into b64, and append result to encoded string
        tmp[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
      } while (i < data.length);

      var enc = tmp.join('');
      var r = data.length % 3;
      return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
    }
  })
  .run(function($state, $rootScope, auth) {
    $rootScope.$on('$stateChangeStart', function(e, target) {
      if (!auth.canAccess(target)) {
        e.preventDefault();
        $state.go('auth.login');
      }
    });
  });