'use strict';

angular.module('backofficeApp')
  .controller('LoginCtrl', function ($scope, $state, auth) {
    $scope.form = {};
    $scope.error = false;

    $scope.submit = function() {
      $scope.error = false;

      auth
      .login($scope.form)
      .then(function() {
          $state.go('main');
      }, function(err) {
          $scope.error = true;
      });
    };
  });
