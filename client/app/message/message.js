'use strict';

angular.module('backofficeApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('message', {
                url: '/message',
                templateUrl: 'app/message/message.html',
                controller: 'MessageCtrl as ctrl'
            });
    })
    .factory('Message', function ($resource, settings) {
        return $resource(settings.apiBaseUrl + '/admin/message');
    });

