'use strict';

angular.module('backofficeApp')
    .controller('MessageCtrl', function (Message) {
        var ctrl = this;

        this.form = {};
        this.send = function () {
            this.sentMessage = this.form.text;
            var message = new Message(ctrl.form);
            message.$save(function() {
                ctrl.sentMessage = ctrl.form.text;
                ctrl.form.text = '';
            });
        };
    });
