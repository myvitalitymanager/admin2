'use strict';

angular.module('backofficeApp')
    .controller('TreeImportCtrl', function($scope, Content) {
        $scope.model = {
            importType: 'append',
            fileType: undefined,
            data: undefined
        };

        $scope.possibleFileTypes = {
            single: 'Old language file (German content only)',
            single_en: 'Old language file (English content only)',
            multi: 'New language file (multiple languages)'
        };

        $scope.import = function() {
            $scope.state = 'loading';

            Content.batchimport($scope.model)
                .$promise
                .then(function() { $scope.state = 'done'; })
                .catch(function() { $scope.state = 'failed'; });
        };
    });
