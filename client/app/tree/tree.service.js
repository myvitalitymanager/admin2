'use strict';
angular.module('backofficeApp')
    .service('tree', function($q, $http, settings, auth) {

        var initialTreeFetch = false;
        var api_url = settings.apiBaseUrl;
        var user = auth.getAuthenticatedUser();

        return {
            getTree: function() {
                return $http.get(api_url + "/admin/content", {
                    cache: false
                }).then(function(result) {
                    initialTreeFetch = true;
                    return result.data;
                });
            },

            getNode: function(id) {

                var deferred = $q.defer();
                var param = {
                    node_id: id
                };

                $http({
                    url: api_url + "/admin/content/node/status",
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: param
                }).then(function(response) {
                        deferred.resolve(response);
                    },
                    function(error) {
                        deferred.resolve("NOT_OK");
                    }
                );
                return deferred.promise;
            },

            deleteNode: function(data) {

                var deferred = $q.defer();
                var param = { id: data, auth_user: user};

                $http({
                    url: api_url + "/admin/content/node/delete",
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: param
                }).then(function(response){
                        deferred.resolve("OK");
                },
                   function(error){
                        deferred.resolve("NOT_OK");
                   }
                );
                return deferred.promise;
            },

            deleteSubTree: function(data) {

                var deferred = $q.defer();
                var param = { path: data, auth_user: user, subtree: true};

                $http({
                    url: api_url + "/admin/content/node/delete",
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: param
                }).then(function(response){
                        deferred.resolve("OK");
                    },
                    function(error){
                        deferred.resolve("NOT_OK");
                    }
                );
                return deferred.promise;
            },

            addNode: function(data) {

                var deferred = $q.defer();
                var param = {
                    path: data,
                    auth_user: user
                };

                $http({
                    url: api_url + "/admin/content/node/create",
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: param
                }).then(function(response) {
                        deferred.resolve("OK");
                    },
                    function(error) {
                        deferred.resolve("NOT_OK");
                    }
                );
                return deferred.promise;
            },

            updateNodeState: function(searchid,newstate) {
                var deferred = $q.defer();

                $http({
                    url: api_url + "/admin/content",
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {id: searchid, state: newstate, auth_user: user}
                }).then(function(response) {
                        deferred.resolve("OK")
                    },
                    function(error) {
                        deferred.resolve("NOT_OK")
                    }
                );
                return deferred.promise;
            },

            updateTreeState: function(treepath,newstate) {
                var deferred = $q.defer();


                $http({
                    url: api_url + "/admin/content/tree",
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {path: treepath, state: newstate, auth_user: user}
                }).then(function(response) {
                        deferred.resolve("OK")
                    },
                    function(error) {
                        deferred.resolve("NOT_OK")
                    }
                );
                return deferred.promise;
            },

            saveNode: function(node) {
                var deferred = $q.defer();

                $http({
                    url: api_url + "/admin/content/node",
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: node
                }).success(function(response) {
                        node.id = response.id;
                        node.version = response.version;
                        deferred.resolve("OK")
                    },
                    function(error) {
                        deferred.resolve("NOT_OK")
                    }
                );
                return deferred.promise;
            },

            getFilteredTree: function(data, state) {

                 var deferred = $q.defer();
                 var param = {
                        filter: data,
                        state: state
                            };

                $http({
                    url: api_url + "/admin/content/node/filter",
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: param
                }).success(function(response) {
                        deferred.resolve(response)
                    },
                    function(error) {
                        deferred.resolve("NOT_OK")
                    }
                );
                return deferred.promise;

            },

            getHistory: function() {
                return $http.get(api_url, {
                    cache: false
                }).then(function(result) {
                    return result.data;
                });
            },

            getStatus: function() {
                return $http.get(api_url, {
                    cache: false
                }).then(function(result) {
                    return result.data;
                });
            },
            isTreeFetched: function() {
                return initialTreeFetch;

            }
        }
    });
