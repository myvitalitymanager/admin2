'use strict';

angular.module('backofficeApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('tree', {
                url: '/tree',
                templateUrl: 'app/tree/tree.html',
                controller: 'TreeCtrl'
            })
            .state('tree.import', {
                url: '/import',
                views: {
                    '@': {
                        templateUrl: 'app/tree/import.html',
                        controller: 'TreeImportCtrl'
                    }
                }
            })
            .state('tree.export', {
                url: '/export',
                views: {
                    '@': {
                        templateUrl: 'app/tree/export.html',
                        controller: 'TreeExportCtrl'
                    }
                }
            });
    })

    .factory('Content', function($resource, settings) {
        return $resource(settings.apiBaseUrl + '/admin/content/:id', { id: '@id' }, {
            batchimport: { method: 'PUT' }
        });
    })

;
