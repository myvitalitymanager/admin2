'use strict';

angular.module('backofficeApp')
    .controller('TreeExportCtrl', function($scope, Content) {
        $scope.model = {
            legacy: false,
            data: undefined
        };

        $scope.export = function() {
            var format = $scope.model.exportType
            $scope.state = 'loading';

            Content.get({ format: 'export', legacy: $scope.model.legacy })
                .$promise
                .then(function(content) {
                    $scope.state      = 'done';
                    $scope.model.data = JSON.stringify(content, null, '    ');
                })
                .catch(function() {
                    $scope.state      = 'failed';
                    $scope.model.data = undefined;
                });
        };
    });
