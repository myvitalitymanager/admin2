'use strict';

angular.module('backofficeApp')
    .controller('TreeCtrl', function($scope, $q, tree, settings, auth) {

        $scope.user = auth.getAuthenticatedUser();

        $scope.system = {
            treeData: undefined
        };

        tree.getTree().then(function(data) {
            buildTree(data);
        });

        $scope.dbFilter = "";

        $scope.myTreeOptions = {
            nodeChildren: "children",
            dirSelectable: true,
            injectClasses: {
                ul: "a1",
                li: "a2",
                liSelected: "a7",
                iExpanded: "a3",
                iCollapsed: "a4",
                iLeaf: "a5",
                label: "a6",
                labelSelected: "a8"
            }
        }

        $scope.menuOptions = [];

        if ($scope.user.canAdminister) {
            $scope.menuOptions.push(
                ['Add Node', function() {

                    var param = $scope.node.path;
                    console.log(param);
                    tree.addNode(param).then(function(data) {
                        tree.getFilteredTree($scope.dbFilter).then(function(data) {
                            buildTree(data);
                        });
                    });
                }],
                null, ['Delete Node', function($node) {
                    var param = $scope.node.id;

                    tree.deleteNode(param).then(function(data) {
                        tree.getFilteredTree($scope.dbFilter).then(function(data) {
                            buildTree(data);
                        });
                    });
                }],
                null, ['Delete Sub-Tree', function($node) {
                    var param = $scope.node.path;
                    tree.deleteSubTree(param).then(function(data) {
                        tree.getFilteredTree($scope.dbFilter).then(function(data) {
                            buildTree(data);
                        });
                    });
                }]
            );
        }

        $scope.menuOptions.push(
            null, ['Expand Sub-Tree', function($node) {
                var keys = [];
                $scope.getTreeKeys(keys, $node);
                //for(var i=0; i<keys.length; i++)
                //    console.log(keys[i][0] + '=' + keys[i][1]);
                $scope.expandedNodes = keys;
            }]
        );

            //null, ['Unpublish', function($node) {
            //    $scope.selected = $scope.node.key;
            //    if ($scope.node1.leaf) {
            //        tree.updateNodeState($scope.node.id, 1).then(function (data) {
            //            $scope.node1.state = "1";
            //            $scope.node.state = "1";
            //        });
            //    } else {
            //        tree.updateTreeState($scope.node.path, 1).then(function (data) {
            //            $scope.system.treeData = "";
            //            $scope.node = undefined;
            //            tree.getFilteredTree($scope.dbFilter).then(function (data) {
            //                buildTree(data);
            //            });
            //        });
            //    }
            //}],
            //null, ['In work', function($node) {
            //    $scope.selected = $scope.node.key;
            //
            //    if ($scope.node1.leaf) {
            //        tree.updateNodeState($scope.node.id, 2).then(function(data) {
            //            $scope.node1.state = "2";
            //            $scope.node.state = "2";
            //        });
            //    } else {
            //        tree.updateTreeState($scope.node.path, 2).then(function(data) {
            //            $scope.system.treeData = "";
            //            $scope.node = undefined;
            //            tree.getFilteredTree($scope.dbFilter).then(function(data) {
            //                buildTree(data);
            //            });
            //        });
            //    }
            //}],
            //null, ['Publish', function($node) {
            //    $scope.selected = $scope.node.key;
            //
            //    if ($scope.node1.leaf) {
            //        tree.updateNodeState($scope.node.id, 3).then(function(data) {
            //            $scope.node1.state = "3";
            //            $scope.node.state = "3";
            //
            //        });
            //    } else {
            //        tree.updateTreeState($scope.node.path, 3).then(function(data) {
            //            $scope.system.treeData = "";
            //             $scope.node = undefined;
            //            tree.getFilteredTree($scope.dbFilter).then(function(data) {
            //                buildTree(data);
            //            });
            //        });
            //    }
            //}]
        //];

        function buildTree(content) {

            var data = content;
            var treeElem;
            var tree = {};
            var aclList = [];
            if ($scope.user.contentsAclPathList) {
                aclList = $scope.user.contentsAclPathList.split(',');
            }

            for (var i = 0; i < data.length; i++) {
                var steps = data[i].path.split('.');

                var publishState = data[i].state.toString();
                var name = data[i].state.toString();
                var thatid = data[i].id;
                var nodepath = data[i].path;
                var editor_comment = data[i].editor_comment;

                // loop over
                var langValues=[];
                settings.languages.forEach (function(element) {
                    var langKey = "value_" +element;
                    langValues.push({
                        "language": element,
                        "content": data[i][langKey]});
                })

                steps.push(data[i].name);
                var current = tree;
                var treepath = "";

                for (var j = 0; j < steps.length; j++) {
                    var step = steps[j];
                    treepath = treepath + step + ".";
                    var process = true; // assume we add it to the tree
                    if (aclList && aclList.length > 0) {
                        process = false; // assume no access
                        aclList.forEach(function(item) {
                            // Do something with `item`
                            item = item.trim();
                            if ((treepath.indexOf(item) == 0) || (item.indexOf(treepath) == 0)) {
                                process = true;
                            }
                        });
                    }
                    if (process) {
                        current.leaf = false;
                        current.children = current.children || {};
                        current = current.children;
                        //console.log("Processing treepath:"+ treepath + " id:" + thatid);
                        if (typeof current[treepath] === "undefined") {
                            if (j === steps.length - 1) {
                                treeElem = {
                                    key: step,
                                    id: thatid,
                                    path: treepath.slice(0, -1),
                                    state: publishState, // todo handle state up the tree path
                                    values: langValues,
                                    editor_comment: editor_comment,
                                    leaf: true,
                                    changed: false
                                };
                            } else {
                                // use empty content nodes for tree nodes with no database entry
                                var emptyLangValues = [];
                                settings.languages.forEach(function (element) {
                                    emptyLangValues.push({
                                        "language": element,
                                        "content": ""
                                    });
                                });
                                treeElem = {
                                    key: step,
                                    path: treepath.slice(0, -1),
                                    state: publishState,
                                    values: emptyLangValues,
                                    editor_comment: editor_comment,
                                    leaf: false,
                                    changed: false
                                };
                            }
                        } else {
                            treeElem = current[treepath];
                            //console.log("j="+j+" - steps.length="+steps.length + " LEAF:" + treeElem.leaf);
                            if (treeElem.leaf == false && (j == (steps.length - 1))) {
                                treeElem.id = thatid;
                                treeElem.values = langValues;
                                treeElem.state = publishState;
                                treeElem.leaf = true;
                            }
                        }
                        current[treepath] = treeElem;
                        current = current[treepath];
                    }
                }
            }
            $scope.system.treeData = tree;
        }


        function deepNodeAdd(tree, obj) {
            _.each(tree, function(item) {
                if (item.id === obj) {

                    if (item.children) {
                        item.children.push(
                            generateNewNode()
                        )
                        item.type = "category";
                    }
                }
                if (item.children) {
                    deepNodeAdd(item.children, obj);
                }
            });
        }


        function deepNodeDelete(tree, obj) {
            _.each(tree, function(item) {
                if (item.id === obj && item.children.length == 0) {
                    for (var i = 0; i < tree.length; i++) {
                        if (tree[i].id === obj) {
                            tree.splice(i, 1);
                            $scope.node.isVisible = false;
                            return;
                        }
                    }
                }
                if (item.children) {
                    deepNodeDelete(item.children, obj);
                }

            });
        }

        function switchStateTo(tree, obj, def) {

            var deferred = def || $q.defer();

            _.each(tree, function(item) {
                item.state = obj;
                if (item.children) {
                    switchStateTo(item.children, obj, def);
                }
            });

            deferred.resolve(tree);

            return deferred.promise;
        }

        function getParentState(tree, obj, def) {

            var deferred = def || $q.defer();
            var resolved = false;

            _.each(tree, function(item) {
                for (var i = 0; i < item.children.length; i++) {
                    if (item.children[i].id === obj) {
                        deferred.resolve(item.state);
                        resolved = true;
                        return;
                    }
                }
                if (item.children) {
                    getParentState(item.children, obj, deferred);
                }
            });

            if (!resolved) {
                deferred.resolve("-1");
            }

            return deferred.promise;
        }

        function isAllowedToSwitchState(parent, child) {

            var parent = parseInt(parent);
            var child = parseInt(child);
            var result = false;


            if (child === 2) {
                result = true;
            } else if (parent === -1) {
                result = true;
            } else if (parent == child) {
                result = true;
            } else if (parent < child) {
                result = false;
            } else if (parent > child) {
                result = true;
            }

            return result;
        }

        function generateNewNode() {
            return {
                "key": "new_" + +(Math.random() * (1000 - 1)) + 1,
                "id": "random_" + (Math.random() * (1000 - 1)) + 1,
                "children": [],
                "type": "node",
                "state": "1",
                "values": [{
                    "language": "de",
                    "content": "Bitte deutschen Text eingeben"
                }, {
                    "language": "en",
                    "content": "Bitte englischen Text eingeben"
                }]
            }
        }

        function generateNewCategory() {
            return {
                "key": "new_" + +(Math.random() * (1000 - 1)) + 1,
                "id": "random" + (Math.random() * (1000 - 1)) + 1,
                "children": [],
                "type": "category",
                "state": "1"
            }
        }

        function Filtering($scope) {
            $scope.predicate = "Node 1";
            $scope.comparator = false;
        }

        $scope.showSelected = function(node) {

            $scope.node = {
                type: node.type,
                path: node.path,
                leaf: node.leaf,
                isVisible: true,
                showHistory: true,
                showStatus: false,
                id: node.id,
                state: node.state,
                key: node.key,
                editor_comment: node.editor_omment,
                values: node.values,
                children: node.children
            };
        };

        $scope.setStyle = function(node) {

            if (typeof node.state != 'undefined') {
                if (node.state === "1") {
                    console.log("unpublished")
                    return {
                      color: '#FF0000'
                    };
                } else if (node.state === "2") {
                    console.log("inwork")
                    return {
                        color: '#f0ad4e'
                    };
                } else if (node.state === "3") {
                    console.log("published")
                    return {
                        color: '#5cb85c'
                    };
                } else if (node.state === "4") {
                    console.log("ready")
                    return {
                        color: '#3071a9'
                    };
                }
            }
            return {
                color: '#000000'
            };
        };

        $scope.getNodeStatus = function(node) {
            if (node && typeof node.state) {
                if (node.state === "1") {
                  return "#FF0000";
                } else if (node.state === "4") {
                    return "#3071a9";
                } else if (node.state === "2") {
                  return "#f0ad4e";
                } else if (node.state === "3") {
                  return "#5cb85c";
                }
            }
            return "#000000"
        };

        $scope.getStatus = function() {

             if(!$scope.node.showStatus){

                tree.getNode($scope.node.id).then(function(result) {

                    var data = result.data;

                    $scope.node.showStatus = true;

                    $scope.nodeInfo = {
                        created_at : data.createdAt,
                        created_by : data.created_by,
                        updated_at : data.updatedAt,
                        updated_by : data.updated_by,
                        published_at : data.published_at,
                        published_by: data.published_by
                    };
            });
             }else{
                $scope.node.showStatus = false;
             }
        };

        $scope.getFilteredTree = function() {

            var filter = $scope.dbFilter;
            var state = $scope.dbState;

            var param = filter.split(/[ ,]+/).join('.')

            tree.getFilteredTree(param, state).then(function(data) {
                buildTree(data);
            });
        };

        $scope.getExpandedFilteredTree = function() {
            $scope.getFilteredTree();
            $scope.setExpandedAll();
        }

        $scope.addNode = function(key) {
            deepNodeAdd($scope.dataForTheTree, key);
        };

        $scope.isNode = function() {
            if ($scope.node.leaf) {
                return true;
            } else {
                return false;
            }
        };

        $scope.deleteNode = function(key) {
            //deepNodeDelete($scope.dataForTheTree, key);
        };


        $scope.saveNode = function() {

            tree.updateNodeState($scope.node.id, 1).then(function(data) {
              $scope.node1.state = "1";
              $scope.node.state = "1";
            });

            tree.saveNode($scope.node).then(function(data) {
                var key = $scope.node.key;
                $scope.node1.key = key;
                node.changed = false;
            });
        };

        $scope.saveListNode = function(node) {
          console.log("SAVE "+ node.id);
            tree.updateNodeState(node.id, 1).then(function(data) {
              node.state = "1";
            });
              tree.saveNode(node).then(function(data) {
              var key = node.key;
                  tree.getFilteredTree($scope.dbFilter).then(function(data) {
                      buildTree(data);
                  });
            });
          node.changed = false;
        };

        $scope.publishNode = function(node) {
            console.log("PUBLISH "+ node.id);
            tree.updateNodeState(node.id, 3).then(function (data) {
                var key = node.key;
                node.state = "3";
            });
        }

        $scope.markNode = function(node) {
            console.log("MARK "+ node.id);
            tree.updateNodeState(node.id, 2).then(function (data) {
                var key = node.key;
                node.state = "2";
            }).then(function(data) {
                tree.saveNode(node).then(function (data) {
                    var key = node.key;
                    node.changed = false;
                    tree.getFilteredTree($scope.dbFilter).then(function (data) {
                        buildTree(data);
                    });
                });
            });
        }

        $scope.readyNode = function(node) {
            console.log("MARK "+ node.id);
            tree.updateNodeState(node.id, 4).then(function (data) {
                var key = node.key;
                node.state = "4";
            });
        }

        $scope.setExpandedAll = function() {
            var keys = [];
            $scope.getKeys(keys, $scope.system.treeData, '');
            $scope.expandedNodes = keys;
        };


        $scope.getKeys = function(keys, obj, path) {
          for(var key in obj) {
            var currpath = path+'/'+key;
            keys.push([key, currpath]);
            if(typeof(obj[key]) == 'object' && !(obj[key] instanceof Array))
              $scope.getKeys(keys, obj[key], currpath);
          }
        }

        $scope.getTreeKeys = function(keys, tree) {
            _.each(tree, function(item) {
                console.log(": "+ item);
                if (item && item.children) {
                        $scope.getTreeKeys(keys, item.children);
                }
            });
        }

        $scope.setCollapsedAll = function() {
            //TODO: rework
            $scope.expandedNodes = [];

        };

        $scope.clearFilter = function() {
            $scope.dbFilter = "";
            tree.getFilteredTree($scope.dbFilter).then(function(data) {
                buildTree(data);
            });

        };

    });
