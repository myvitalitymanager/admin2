'use strict';

angular.module('backofficeApp')
    .config(function($stateProvider) {
        $stateProvider
        .state('access', {
                abstract: true,
                url: '/access',
                template: '<ui-view/>'
        })
        .state('access.list', {
            url: '/list',
            templateUrl: 'app/access/list.html',
            controller: 'AccessListCtrl',
            resolve: {
                list: function($q, AdminUser) {
                    console.log("ere");
                    var deferred = $q.defer();
                    AdminUser.query(function(res) {
                        deferred.resolve(res);
                    }, function(err) {
                        deferred.reject(err);
                    });
                    return deferred.promise;
                }
            }
        });
    })
    .factory('AdminUser', function($resource, settings) {
        return $resource(
            settings.apiBaseUrl + '/admin/admin-users/:id',
            {'id': '@id'},
            {update: {method: 'PUT'}}
        );
    });
