'use strict';

angular.module('backofficeApp')
  .service('adminDialog', function ($rootScope, $q, $modal, AdminUser) {
    this.show = function(admin) {
        var deferred = $q.defer();
        var modal = null;
        var scope = $rootScope.$new();

        scope.admin = angular.copy(admin || {});
        scope.admin.password = null;

        scope.save = function() {
            var admin = scope.admin;

            if (admin.id) {
                admin.$update(success, error);
            } else {
                admin = new AdminUser(admin);
                admin.$save(success, error);
            }

            function success() {
                modal.dismiss();
                deferred.resolve(admin);
            }

            function error(err) {
                scope.error = err.data;
            }
        };

        modal = $modal.open({
            templateUrl: 'app/access/edit.html',
            scope: scope
        });

        return deferred.promise;
    }
  });
