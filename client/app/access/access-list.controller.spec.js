'use strict';

describe('Controller: AccessListCtrl', function () {

  // load the controller's module
  beforeEach(module('backofficeApp'));

  var AccessListCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AccessListCtrl = $controller('AccessListCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
