'use strict';

angular.module('backofficeApp')
  .controller('AccessListCtrl', function ($scope, adminDialog, list, AdminUser) {
    $scope.list = list;

    $scope.add = function() {
        adminDialog.show()
        .then(function(user) {
            $scope.list.push(user);
        });
    }

    $scope.edit = function(user) {
        adminDialog.show(user)
        .then(function(updatedUser) {
            angular.extend(user, updatedUser);
        });
    }
  });
