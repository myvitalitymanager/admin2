'use strict';

angular.module('backofficeApp')
  .config(function(settingsProvider) {
    settingsProvider.settings.servers.unshift({
      name: 'Dev',
      apiBaseUrl: 'http://localhost:8080'
    });
  })