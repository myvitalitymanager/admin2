'use strict';

angular.module('backofficeApp')
  .controller('DataUsersCtrl', function ($scope, $stateParams, $rootScope, $modal, $q, User, Account, tree) {
    $scope.rows = [];
    $scope.isLoading = false;

    $scope.update = function(params, ctrl) {
      $scope.isLoading = true;
      var query = {
        offset: params.pagination.start || 0,
        limit: params.pagination.number || 10,
      };
      if (params.search.predicateObject) {
        query = angular.extend(query, params.search.predicateObject);
      }

      User.query(query, function(res, headers) {
        var max = parseInt(headers('x-list-max'));
        var limit = parseInt(headers('x-list-limit'));
        params.pagination.numberOfPages = Math.ceil(max/limit);
        $scope.rows = res;
        $scope.isLoading = false;
      }, function(err) {
        console.log(err);
        $scope.isLoading = false;
      });
    };

    $scope.edit = function(row) {
      var modal = null;
      var scope = $rootScope.$new();
      scope.model = angular.copy(row);
      scope.schema = {
        type: 'object',
        properties: {
          firstName: {type: 'string'},
          lastName: {type: 'string'},
          email: {type: 'string'},
          password: {type: 'string'}
        }
      };
      scope.form = ['*', {
        type: "submit",
        title: "Save"
      }];

      scope.save = function() {
        var deferred = $q.defer();
        console.log(row);

        User.update({
          id: row.id,
          firstName: scope.model.firstName,
          lastName: scope.model.lastName,
          email: scope.model.email
        }, function(res) {
          row.firstName = res.firstName;
          row.lastName = res.lastName;
          row.displayName = res.displayName;
          row.email = res.email;

          if (scope.model.password) {
            var account = _.find(row.accounts, {provider: 'local'});
            if (account) {
              Account.update({
                id: account.id,
                authSecret: scope.model.password
              }, function() {
                deferred.resolve();
              }, function(err) {
                deferred.reject(err);
              });
            } else {
              deferred.resolve();
            }
          } else {
            deferred.resolve();
          }
        }, function(err) {
          deferred.reject(err);
        });

        deferred.promise.then(function() {
          modal.dismiss();
        }, function(err) {
          console.log(err);
          modal.dismiss();
        });
      };

      modal = $modal.open({
        templateUrl: 'app/data/edit.html',
        scope: scope
      });
    };
  });
