'use strict';

angular.module('backofficeApp')
    .config(function($stateProvider) {
        $stateProvider
        .state('data', {
            url: '/data',
            templateUrl: 'app/data/menu/menu.html',
            controller: 'DataMenuCtrl'
        })
        .state('data.users', {
            url: '/users',
            templateUrl: 'app/data/users/users.html',
            controller: 'DataUsersCtrl'
        });
    })
    .factory('User', function($resource, settings) {
        return $resource(settings.apiBaseUrl + '/admin/data/users/:id', {id: '@id'}, {
            update: { method: 'PUT' }
        });
    })
    .factory('Account', function($resource, settings) {
        return $resource(settings.apiBaseUrl + '/admin/data/accounts/:id', {id: '@id'}, {
            update: { method: 'PUT' }
        });
    });
