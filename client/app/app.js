'use strict';

angular.module('backofficeApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngStorage',
  'ui.router',
  'treeControl',
  'ui.tree',
  'pageslide-directive',
  'ui.bootstrap.contextMenu',
  'ui.bootstrap',
  'schemaForm',
  'smart-table',
  'contenteditable',
  'monospaced.elastic'
])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(false);
  });
