'use strict';

angular.module('backofficeApp')
    .config(function($stateProvider, schemaFormDecoratorsProvider) {
        $stateProvider
        .state('modules', {
            url: '/modules',
            templateUrl: 'app/modules/menu/menu.html',
            controller: 'ModuleMenuCtrl',
            resolve: {
                modules: function($q, ModuleType) {
                    var deferred = $q.defer();

                    ModuleType.query(function(res) {
                        deferred.resolve(res);
                    }, function(err) {
                        deferred.reject(err);
                    });

                    return deferred.promise;
                }
            }
        })
        .state('modules.new', {
            url: '/edit',
            templateUrl: 'app/modules/edit/edit.html',
            controller: 'ModuleEditCtrl',
            resolve: {
                module: function() {
                    return null;
                }
            }
        })
        .state('modules.edit', {
            url: '/edit/:id',
            templateUrl: 'app/modules/edit/edit.html',
            controller: 'ModuleEditCtrl',
            resolve: {
                module: function(modules, $stateParams) {
                    return _.find(modules, {id: $stateParams.id});
                }
            }
        });

        schemaFormDecoratorsProvider.addMapping(
            'bootstrapDecorator',
            'values',
            'app/modules/edit/values.html'
        );
    })
    .factory('ModuleType', function($resource, settings) {
        return $resource(settings.apiBaseUrl + '/admin/data/module-types/:id', {id: '@id'}, {
            update: { method: 'PUT' }
        });
    });
