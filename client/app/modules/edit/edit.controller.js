'use strict';

angular.module('backofficeApp')
  .controller('ModuleEditCtrl', function ($scope, $anchorScroll, $timeout, $state, ModuleType, moduleDialogService, settings, modules, module) {
    var TASK_TYPE_REGEXP = /^\w*?(\d+).*$/;
    var resolutions = settings.quicktest.resolutions.values;
    var parameterNames = Object.keys(settings.quicktest).filter(function(key) {
        return settings.quicktest[key].mod;
    });
    var parameters = {};
    parameterNames.forEach(function(key) {
        parameters[key] = angular.extend({}, settings.quicktest[key]);
    });
    sortModuleTasks(module);

    function sortModuleTasks(module) {
        if (module && angular.isArray(module.tasks)) {
            module.tasks.forEach(function (taskType) {
                var match = TASK_TYPE_REGEXP.exec(taskType.type);
                if (match) {
                    taskType.order = parseInt(match[1], 10);
                } else {
                    taskType.order = 0;
                }
            });
            module.tasks.sort(function (a, b) {
                return a.order > b.order ? 1 : (a.order < b.order ? -1 : 0);
            });
        }
    }

    function findMaxOrder(tasks) {
        return tasks.reduce(function (maxOrder, taskType) {
            var match = TASK_TYPE_REGEXP.exec(taskType.type);
            if (match) {
                return Math.max(maxOrder, parseInt(match[1], 10));
            }
            return maxOrder;
        }, 0);
    }

    $scope.schema = {
        type: 'object',
        properties: {
            type: {
                type: 'string',
                title: 'Key (letters, numbers, underscore)'
            },
            duration: {
                type: 'number',
                title: 'Duration in days'
            },
            isActive: {
                type: 'boolean',
                title: 'Active'
            },
            isFree: {
                type: 'boolean',
                title: 'Free'
            },
            imageUrl: {
                type: 'string',
                title: 'Image URL (app-local or aws)'
            },
            rankingSettings: {
                type: 'object',
                properties: {
                    resolutions: {
                        type: 'array',
                        items: {
                            type: 'string',
                            enum: resolutions
                        }
                    },
                    suitable: {
                        type: 'array',
                        items: {
                            type: 'values'
                        }
                    },
                    effects: {
                        type: 'array',
                        items: {
                            type: 'object',
                            properties: {
                                parameter: {
                                    type: 'string',
                                    enum: parameterNames
                                },
                                value: {
                                    type: 'string'
                                }
                            }
                        }
                    }
                }
            },
            products: {
                type: 'array',
                items: {
                    type: 'object',
                    properties: {
                        productUrl: {type: 'string'}
                    }
                }
            },
            tasks: {
                type: 'array',
                items: {
                    type: 'object',
                    properties: {
                        type: {
                            type: 'string'
                        },
                        isYesNo: {
                            type: 'boolean'
                        },
                        reminderSettings: {
                            type: 'array',
                            items: {
                                type: 'string',
                                enum: ['morning', 'lunch', 'evening']
                            }
                        }
                    }
                }
            }
        }
    };
    $scope.form = [
        'type',
        'isActive',
        'isFree',
        'duration',
        'imageUrl',
        {
            type: 'submit',
            title: 'Save'
        },
        {
            type: 'tabs',
            tabs: [
                {
                    title: 'Ranking',
                    items: [{
                        key: 'rankingSettings',
                        title: 'Effective with resolutions:',
                        items: [{
                            key: 'rankingSettings.resolutions',
                            notitle: true
                        }, {
                            key: 'rankingSettings.suitable',
                            title: 'Parameter',
                            htmlClass: 'secondary',
                            items: [{
                                key: 'rankingSettings.suitable[]',
                                type: 'values',
                                parameters: parameters
                            }]
                        }, {
                            key: 'rankingSettings.effects',
                            title: 'Changes in quicktest parameters:',
                            htmlClass: 'secondary',
                            items: [{
                                key: 'rankingSettings.effects[]',
                                notitle: true,
                                title: '',
                                items: [{
                                    key: 'rankingSettings.effects[].parameter',
                                    title: 'Parameter'
                                }, {
                                    key: 'rankingSettings.effects[].value',
                                    title: 'Delta'
                                }]
                            }]
                        }]
                    }]
                },
                {
                    title: 'Tasks',
                    items: [{
                        key: 'tasks',
                        notitle: true,
                        items: [{
                            key: 'tasks[]',
                            notitle: true,
                            title: '',
                            items: [{
                                key: 'tasks[].type',
                                title: 'Key (letters, numbers, underscore)'
                            }, {
                                key: 'tasks[].isYesNo',
                                title: 'Answer is Yes/No'
                            },
                                {
                                key: 'tasks[].reminderSettings',
                                title: 'Active reminders'
                            }]
                        }]
                    }]
                },
                {
                    title: 'Products',
                    items: [{
                        key: 'products',
                        notitle: true,
                        items: [{
                            key: 'products[].productUrl',
                            title: 'Amazon Produkt ID'
                        }]
                    }]
                }
            ]
        },
        {
            type: 'submit',
            title: 'Save'
        }
    ];
    $scope.model = module || {
        type: '',
        duration: 0,
        isActive: false,
        isFree: false,
        imageUrl: '',
        rankingSettings: {},
        products: [],
        tasks: []
    };

    $scope.save = function() {
        $scope.model.tasks = $scope.model.tasks.filter(function(task) {
            return task.type;
        });
        $scope.model.tasks.forEach(function (taskType) {
            var match = TASK_TYPE_REGEXP.exec(taskType.type);
            if (match) {
                taskType.order = parseInt(match[1], 10);
            } else {
                taskType.order = 0;
            }
        });

        if ($scope.model.id) {
            $scope.model.$update(success, error);
        } else {
            var type = new ModuleType($scope.model);
            type.$save(success, error);
        }

        function success(res) {
            if (!$scope.model.id) {
                $scope.model = res;
                modules.push(res);
                $state.go('^.edit', {id: res.id})
                .then(function() {
                    $scope.showSaved();
                });
            } else {
                $scope.showSaved();
            }
        }
        function error(err) {
            $scope.showError(err);
        }
    };

    $scope.showSaved = function() {
        $anchorScroll();
        $scope.saved = true;
        $timeout(function() {
            $scope.saved = false;
        }, 3000);
    };

    $scope.showError = function(error) {
        $scope.error = error.data;
    };

    $scope.hideError = function() {
        $scope.error = null;
    };

    $scope.$watchCollection('model.tasks', function (newTasks, oldTasks) {
        if (newTasks === oldTasks) {
            return;
        }
        var maxOrder = findMaxOrder(newTasks);
        newTasks.forEach(function (task) {
            if (!task.type) {
                ++maxOrder;
                task.type = 'task' + maxOrder;
            }
        });
    });
  });
