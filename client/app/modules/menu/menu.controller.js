'use strict';

angular.module('backofficeApp')
  .controller('ModuleMenuCtrl', function ($scope, $http, settings, modules) {
    $scope.modules = modules;

    $scope.isActive = true;

    $scope.testMode = false;

    $scope.testRanking = function(testForm) {
        console.log("Email:" +$scope.email);
        if (!$scope.email) {
            $scope.email = "7";
        }
        $http.get(settings.apiBaseUrl+'/admin/plan/' + $scope.email)
        .then(function(res) {
                $scope.testMode = true;
                $scope.testData = res.data;
            console.log(res.data);
        });
    };

    $scope.toggleIsActive = function() {
        $scope.isActive = !$scope.isActive;
        console.log("ACTIVE: "+ $scope.isActive);
    };

  });
