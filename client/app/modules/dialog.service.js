'use strict';

angular.module('backofficeApp')
  .service('moduleDialogService', function ($modal, $rootScope, $q) {

    this.fromSchema = function(schema, form, model) {
        var modal = null;
        var scope = $rootScope.$new();
        var deferred = $q.defer();

        scope.schema = schema;
        scope.form = form || ['*', {type: 'submit', title: 'Save'}];
        scope.model = model || {};

        scope.save = function() {
            deferred.resolve(scope.model);
            modal.dismiss();
        };

        modal = $modal.open({
            templateUrl: 'app/modules/dialog.html',
            scope: scope
        });

        return deferred.promise;
    };
  });
