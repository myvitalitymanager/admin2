# API image

FROM happitz/base:latest

# Install dependencies
RUN npm install -g grunt-cli bower

# Add a custom happitz user for downgrading the node server to it
RUN groupadd happitz \
    && useradd -d /home/happitz -m -g happitz -s /bin/false happitz

COPY . /home/happitz/admin
WORKDIR /home/happitz/admin

# COPY always copies as root, so chown the api directory before switching to the happitz user.
# This is a workaround until COPY supports creating the files under a different user.
RUN chown -R happitz:happitz .

# Downgrade, install npm dependencies
USER happitz
RUN npm install
RUN bower install

EXPOSE 8100
CMD ["grunt", "serve:dist"]