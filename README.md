# Setup

Install global dependencies, if necessary:

    npm install -g grunt-cli bower

Install local dependencies:

    npm install
    bower install

# Development

    grunt serve
